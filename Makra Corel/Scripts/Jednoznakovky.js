function Jednoznakovky() {
  host.ActiveDocument.Unit = cdrMillimeter

  const selections = host.ActiveSelection.Shapes;

  if (selections.Count > 1) {
    alert('M�te vybr�no v�c ne� jeden objekt!');
    return;
  }

  if (selections.Count < 1) {
    alert('Nen� vybran� ��dn� objekt!');
    return;
  }

  const shape = selections.First;


  if (shape.Type !== cdrTextShape) {
    alert('Vybran� objekt nen� textov� objekt!');
    return;
  }

  const text = shape.Text.Story.Text;

  const replacedText = text.replace(/((^|[^a-�A-�])[szkviouSZKVIOUA])\s+/g, `$1${String.fromCharCode(160)}`);
  shape.Text.Story.Text = replacedText;
}

Jednoznakovky();
